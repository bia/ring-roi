package plugins.fab.ringroi;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import icy.roi.ROI2D;
import icy.sequence.Sequence;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;

public class RingROI extends EzPlug implements Block
{
    EzVarSequence sequenceVar = new EzVarSequence("Input Sequence");
    EzVarBoolean colorRingVar = new EzVarBoolean("Color ring", true);

    @Override
    public void clean()
    {
        //
    }

    @Override
    protected void initialize()
    {
        addEzComponent(sequenceVar);
        addEzComponent(colorRingVar);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", sequenceVar.getVariable());
        inputMap.add("colorRing", colorRingVar.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    static class ROIDescriptor
    {
        double surface;
        ROI2D roi;

        public ROIDescriptor(double roiSurface, ROI2D roiInput)
        {
            this.surface = roiSurface;
            this.roi = roiInput;
        }
    }

    @Override
    protected void execute()
    {
        convertToRingROI(sequenceVar.getValue(), colorRingVar.getValue().booleanValue());
    }

    /**
     * Convert the ROI contained in the Sequence to Ring ROI
     */
    public static void convertToRingROI(Sequence sequence, boolean color)
    {
        final List<ROI2D> ROIList = sequence.getROI2Ds();
        final List<ROI2D> ROIListOutPut = new ArrayList<ROI2D>();
        final List<ROIDescriptor> ROIDescriptorList = new ArrayList<ROIDescriptor>();

        // order ROI by size
        for (ROI2D roiInput : ROIList)
        {
            ROIDescriptor roiDescriptor = new ROIDescriptor(roiInput.getNumberOfPoints(), roiInput);
            ROIDescriptorList.add(roiDescriptor);
        }

        // for each ROI, subtract the most bigger of the smaller existing ROI.
        for (ROI2D roiInput : ROIList)
        {
            ROI2D roiSubstract = getROItoSubstract(ROIDescriptorList, roiInput);

            if (roiSubstract != null)
            {
                ROI2D ring = (ROI2D) roiInput.subtract(roiSubstract, true);
                ROIListOutPut.add(ring);
            }
            else
            {
                ROIListOutPut.add(roiInput);
            }
        }

        if (color)
        {
            float hue = 0;
            for (ROI2D roi : ROIListOutPut)
            {
                hue += 1f / ROIListOutPut.size();
                roi.setColor(Color.getHSBColor(hue, 1, 1));
            }
        }

        sequence.beginUpdate();
        try
        {
            sequence.removeAllROI(true);
            sequence.addROIs(ROIListOutPut, true);
        }
        finally
        {
            sequence.endUpdate();
        }
    }

    private static ROI2D getROItoSubstract(List<ROIDescriptor> descriptors, ROI2D roi)
    {
        double surfaceInput = 0d;

        for (ROIDescriptor descriptor : descriptors)
        {
            if (descriptor.roi == roi)
            {
                surfaceInput = descriptor.surface;
                break;
            }
        }

        double bestSurface = 0d;
        ROI2D bestROI = null;

        for (ROIDescriptor descriptor : descriptors)
        {
            double surface = descriptor.surface; // getROISurface( roi );

            if (surface > bestSurface && surface < surfaceInput)
            {
                bestSurface = surface;
                bestROI = descriptor.roi;
            }
        }

        return bestROI;
    }
}
